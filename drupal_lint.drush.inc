<?php

/**
 * @file
 * Drush command hooks.
 */

// @TODO UI, editorial based checks "comment queue huge" etc.

// Force param

// @TODO checks:
//  foodity connectivity (AOP style).
//  search connectivity.

/**
 * Implements hook_drush_command().
 */
function drupal_lint_drush_command() {
  $items = array();

  $items['lint'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_MAX,
    'description' => t('Lint your configuration'),
  );

  return $items;
}

/**
 * Command call back for the lint command.
 */
function drush_drupal_lint_lint() {

  foreach (_drupal_lint_get_all_checks() as $name => $check) {

    if (!function_exists($check['callback'])) {
      drush_log(dt('Check callback !callback does not exist.', array('!callback' => $check['callback'])), 'error');
      continue;
    }

    $results[$name] = call_user_func($check['callback']);
  }

  drupal_alter('drupal_lint_result_alter', $results);

  foreach ($results as $result) {
    foreach ($result as $message) {
      drush_log($message[0], $message[1]);
    }
  }
}

/**
 * Gather all the check callbacks.
 */
function _drupal_lint_get_all_checks() {

  $checks = module_invoke_all('check');

  drupal_alter('drupal_lint_check_alter', $checks);

  foreach ($checks as $name => $check) {
    if (!isset($check['callback'])) {
      $checks[$name]['callback'] = 'drupal_lint_check_' . str_replace('-', '_', $name);
    }
  }

  return $checks;
}

/**
 * Check if there are any large objects in the variable table.
 */
function drupal_lint_check_fast_404() {
  $results = array();

  if (!module_exists('fast_404') && !isset($conf['404_fast_paths'])) {
    $results['fast-404'] = array(
      dt('Core fast 404 is not configured and the fast 404 module is not enabled.'),
      'warning',
    );
  }

  return array();
}
/**
 * Check if there are any large objects in the variable table.
 */
function drupal_lint_check_core_variable_table_size() {
  $results = array();

  $rows = db_select('variable', 'v')
    ->fields('v')
    ->execute();

  foreach ($rows as $row) {
    // Some hack with memory_get_usage() is probably not worth it.
    if (strlen($row->value) >= variable_get('drupal_lint_large_variable_value', 500)) {
      $results[$row->name] = array(
        dt(
          'Large object found in the variable table: !name',
          array(
            '!name' => $row->name,
          )
        ),
        'warning',
      );
    }
  }

  return $results;
}

/**
 * Check if any views do not have caching enabled.
 */
function drupal_lint_check_views_caching() {

  $results = array();

  foreach (views_get_all_views() as $view_name => $view) {

    if ($view->disabled) {
      continue;
    }

    foreach ($view->display as $display_name => $display) {

      if ($display->display_options['cache']['type'] == 'none') {

        $results[$view_name . '-' . $display_name] = t(
          'Caching disabled for view: !view_name display: !display_name',
          array(
            '!view_name' => $view_name,
            '!display_name' => $display_name,
          )
        );
      }
    }
  }

  return $results;
}

/**
 * Check if any dangerous permissions have been assigned to the anonymous user.
 */
function drupal_lint_check_dangerous_permissions() {
  $results = array();

  $anonymous_access = user_role_permissions(
    array(DRUPAL_ANONYMOUS_RID => DRUPAL_ANONYMOUS_RID)
  );

  foreach (module_invoke_all('permission') as $name => $permission) {
    if ($permission['restrict access'] == TRUE &&
      isset($anonymous_access[DRUPAL_ANONYMOUS_RID][$name])) {

      $results[$name] = array(
        dt(
          'Dangerous permission assigned to the anonymous role: !name',
          array(
            '!name' => $name,
          )
        ),
        'failed',
      );
    }
  }

  return $results;
}

/**
 * Check if any dangerous modules are enabled.
 */
function drupal_lint_check_dangerous_modules() {

  $dangerous_modules = array(
    'devel',
    'php',
  );

  drupal_alter('drupal_lint_check_dangerous_modules', $dangerous_modules);

  $results = array();

  foreach ($dangerous_modules as $module) {
    if (module_exists($module)) {
      $results[$module] = array(
        dt(
          'Dangerous module enabled: !module',
          array(
            '!module' => $module,
          )
        ),
        'warning',
      );
    }
  }

  return $results;
}

/**
 * @TODO!
 */
function drupal_lint_check_aggregation() {
  return array();
}

/**
 * @TODO!
 */
function drupal_lint_check_unperformant_modules() {
  return array();
}
